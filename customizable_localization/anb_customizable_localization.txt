
defined_text = {
	name = GetMagesOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_mages_name
		trigger = {
			has_estate = estate_mages
		}
	}
	
	text = {
		localisation_key = estate_mages
		trigger = {
			NOT = { has_estate = estate_mages }
		}
	}
}

defined_text = {
	name = GetArtificersOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_artificers_name
		trigger = {
			has_estate = estate_artificers
		}
	}
	
	text = {
		localisation_key = estate_artificers
		trigger = {
			NOT = { has_estate = estate_artificers }
		}
	}
}

defined_text = {
	name = GetAdventurersOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_adventurers_name
		trigger = {
			has_estate = estate_adventurers
		}
	}
	
	text = {
		localisation_key = estate_adventurers
		trigger = {
			NOT = { has_estate = estate_adventurers }
		}
	}
}

defined_text = {
	name = GetMonstrousTribesOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_monstrous_tribes_name
		trigger = {
			has_estate = estate_monstrous_tribes
		}
	}
	
	text = {
		localisation_key = estate_monstrous_tribes
		trigger = {
			NOT = { has_estate = estate_monstrous_tribes }
		}
	}
}

defined_text = {
	name = GetRajMinistriesOrFallbackName
	random = no
	
	text = {
		localisation_key = country_estate_raj_ministries_name
		trigger = {
			has_estate = estate_raj_ministries
		}
	}
	
	text = {
		localisation_key = estate_raj_ministries
		trigger = {
			NOT = { has_estate = estate_raj_ministries }
		}
	}
}

defined_text = {
	name = GetHorseRaceFirst
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 1 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceSecond
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 2 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceThird
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 3 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceFourth
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 4 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceFifth
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 5 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceSixth
	random = no
	
	text = {
		localisation_key = horseRaceBlue
		trigger = {
			event_target:blue_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceTeal
		trigger = {
			event_target:teal_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			event_target:green_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceYellow
		trigger = {
			event_target:yellow_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			event_target:orange_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			event_target:red_origin = { is_variable_equal = { which = horseRaceRank value = 6 } }
		}
	}
}

defined_text = {
	name = GetHorseRaceColour
	random = no
	
	text = {
		localisation_key = horseRaceOptionBlue
		trigger = {
			province_id = event_target:blue_origin
		}
	}
	text = {
		localisation_key = horseRaceOptionTeal
		trigger = {
			province_id = event_target:teal_origin
		}
	}
	text = {
		localisation_key = horseRaceGreen
		trigger = {
			province_id = event_target:green_origin
		}
	}
	text = {
		localisation_key = horseRaceOptionYellow
		trigger = {
			province_id = event_target:yellow_origin
		}
	}
	text = {
		localisation_key = horseRaceOrange
		trigger = {
			province_id = event_target:orange_origin
		}
	}
	text = {
		localisation_key = horseRaceRed
		trigger = {
			province_id = event_target:red_origin
		}
	}
}

defined_text = {
	name = GetGhavaanajTrainingFocus
	random = no
	
	text = {
		localisation_key = trainingFocusSwift
		trigger = {
			has_country_flag = ghavaanaj_herd_training_swift_flag
		}
	}
	text = {
		localisation_key = trainingFocusSturdy
		trigger = {
			has_country_flag = ghavaanaj_herd_training_sturdy_flag
		}
	}
	text = {
		localisation_key = trainingFocusSmart
		trigger = {
			has_country_flag = ghavaanaj_herd_training_smart_flag
		}
	}
	text = {
		localisation_key = trainingFocusSteadfast
		trigger = {
			has_country_flag = ghavaanaj_herd_training_steadfast_flag
		}
	}
}

defined_text = {
	name = GetGhavaanajTrainingFocus
	random = no
	
	text = {
		localisation_key = trainingFocusSwift
		trigger = {
			has_country_flag = ghavaanaj_herd_training_swift_flag
		}
	}
	text = {
		localisation_key = trainingFocusSturdy
		trigger = {
			has_country_flag = ghavaanaj_herd_training_sturdy_flag
		}
	}
	text = {
		localisation_key = trainingFocusSmart
		trigger = {
			has_country_flag = ghavaanaj_herd_training_smart_flag
		}
	}
	text = {
		localisation_key = trainingFocusSteadfast
		trigger = {
			has_country_flag = ghavaanaj_herd_training_steadfast_flag
		}
	}
}

defined_text = {
	name = GetGhavaanajTrainingProgress
	random = no
	
	text = {
		localisation_key = nscTensionsBarFull
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 100 }
		}
	}
	text = {
		localisation_key = nscTensionsBar9
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 90 }
		}
	}
	text = {
		localisation_key = nscTensionsBar8
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 80 }
		}
	}
	text = {
		localisation_key = nscTensionsBar7
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 70 }
		}
	}
	text = {
		localisation_key = nscTensionsBar6
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 60 }
		}
	}
	text = {
		localisation_key = nscTensionsBar5
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 50 }
		}
	}
	text = {
		localisation_key = nscTensionsBar4
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 40 }
		}
	}
	text = {
		localisation_key = nscTensionsBar3
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 30 }
		}
	}
	text = {
		localisation_key = nscTensionsBar2
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 20 }
		}
	}
	text = {
		localisation_key = nscTensionsBar1
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 10 }
		}
	}
	text = {
		localisation_key = nscTensionsBar0
		trigger = {
			check_variable = { which = ghavaanaj_herd_training_counter value = 0 }
		}
	}
}

defined_text = {
	name = GetGhavaanajTrainingChance
	random = no
	
	text = {
		localisation_key = ghavaanajTrainingChance
		trigger = { OR = { has_country_flag = ghavaanaj_herd_training_swift_flag has_country_flag = ghavaanaj_herd_training_sturdy_flag has_country_flag = ghavaanaj_herd_training_smart_flag has_country_flag = ghavaanaj_herd_training_steadfast_flag } }
	}
}

defined_text = {
	name = GetGhavaanajTrainingPhysical
	random = no
	
	text = {
		localisation_key = ghavaanajTrainingBar0
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_03 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar1
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_02 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar2
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_01 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar3
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_00 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar4
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_11 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar5
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_12 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar6
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_13 }
	}
}

defined_text = {
	name = GetGhavaanajTrainingMental
	random = no
	
	text = {
		localisation_key = ghavaanajTrainingBar0
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_03 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar1
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_02 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar2
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_01 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar3
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_00 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar4
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_11 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar5
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_12 }
	}
	text = {
		localisation_key = ghavaanajTrainingBar6
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_13 }
	}
}

defined_text = {
	name = GetGhavaanajTrainingMentalBonus
	random = no
	
	text = {
		localisation_key = ghavaanajTrainingMentalBonus03
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_03 }
	}
	text = {
		localisation_key = ghavaanajTrainingMentalBonus02
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_02 }
	}
	text = {
		localisation_key = ghavaanajTrainingMentalBonus01
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_01 }
	}
	text = {
		localisation_key = ghavaanajTrainingMentalBonus00
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_00 }
	}
	text = {
		localisation_key = ghavaanajTrainingMentalBonus11
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_11 }
	}
	text = {
		localisation_key = ghavaanajTrainingMentalBonus12
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_12 }
	}
	text = {
		localisation_key = ghavaanajTrainingMentalBonus13
		trigger = { has_country_modifier = ghavaanaj_herd_training_mental_13 }
	}
}

defined_text = {
	name = GetGhavaanajTrainingPhysicalBonus
	random = no
	
	text = {
		localisation_key = ghavaanajTrainingPhysicalBonus03
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_03 }
	}
	text = {
		localisation_key = ghavaanajTrainingPhysicalBonus02
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_02 }
	}
	text = {
		localisation_key = ghavaanajTrainingPhysicalBonus01
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_01 }
	}
	text = {
		localisation_key = ghavaanajTrainingPhysicalBonus00
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_00 }
	}
	text = {
		localisation_key = ghavaanajTrainingPhysicalBonus11
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_11 }
	}
	text = {
		localisation_key = ghavaanajTrainingPhysicalBonus12
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_12 }
	}
	text = {
		localisation_key = ghavaanajTrainingPhysicalBonus13
		trigger = { has_country_modifier = ghavaanaj_herd_training_physical_13 }
	}
}

defined_text = {
	name = GetGhavaanajHerdSizeNumber
	random = no
	
	text = {
		localisation_key = ghavaanajHerdSizeNumber20
		trigger = { ghavaanaj_has_herd_size_20 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber19
		trigger = { ghavaanaj_has_herd_size_19 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber18
		trigger = { ghavaanaj_has_herd_size_18 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber17
		trigger = { ghavaanaj_has_herd_size_17 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber16
		trigger = { ghavaanaj_has_herd_size_16 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber15
		trigger = { ghavaanaj_has_herd_size_15 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber14
		trigger = { ghavaanaj_has_herd_size_14 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber13
		trigger = { ghavaanaj_has_herd_size_13 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber12
		trigger = { ghavaanaj_has_herd_size_12 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber11
		trigger = { ghavaanaj_has_herd_size_11 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber10
		trigger = { ghavaanaj_has_herd_size_10 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber9
		trigger = { ghavaanaj_has_herd_size_9 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber8
		trigger = { ghavaanaj_has_herd_size_8 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber7
		trigger = { ghavaanaj_has_herd_size_7 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber6
		trigger = { ghavaanaj_has_herd_size_6 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber5
		trigger = { ghavaanaj_has_herd_size_5 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber4
		trigger = { ghavaanaj_has_herd_size_4 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber3
		trigger = { ghavaanaj_has_herd_size_3 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber2
		trigger = { ghavaanaj_has_herd_size_2 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumber1
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSizeNumberHalved
	random = no
	
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved20
		trigger = { ghavaanaj_has_herd_size_20 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved19
		trigger = { ghavaanaj_has_herd_size_19 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved18
		trigger = { ghavaanaj_has_herd_size_18 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved17
		trigger = { ghavaanaj_has_herd_size_17 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved16
		trigger = { ghavaanaj_has_herd_size_16 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved15
		trigger = { ghavaanaj_has_herd_size_15 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved14
		trigger = { ghavaanaj_has_herd_size_14 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved13
		trigger = { ghavaanaj_has_herd_size_13 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved12
		trigger = { ghavaanaj_has_herd_size_12 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved11
		trigger = { ghavaanaj_has_herd_size_11 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved10
		trigger = { ghavaanaj_has_herd_size_10 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved9
		trigger = { ghavaanaj_has_herd_size_9 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved8
		trigger = { ghavaanaj_has_herd_size_8 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved7
		trigger = { ghavaanaj_has_herd_size_7 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved6
		trigger = { ghavaanaj_has_herd_size_6 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved5
		trigger = { ghavaanaj_has_herd_size_5 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved4
		trigger = { ghavaanaj_has_herd_size_4 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved3
		trigger = { ghavaanaj_has_herd_size_3 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved2
		trigger = { ghavaanaj_has_herd_size_2 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdSizeNumberHalved1
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSizeGrowth
	random = no
	
	text = {
		localisation_key = ghavaanajHerdGrowth9
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 9 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth8
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 8 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth7
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 7 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth6
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 6 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth5
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 5 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth4
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 4 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth3
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 3 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth2
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 2 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth1
		trigger = { check_variable = { which = ghavaanaj_herd_size_counter value = 1 } }
	}
	text = {
		localisation_key = ghavaanajHerdGrowth0
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize1
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { always = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 1 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize2
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_2 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 2 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize3
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_3 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 3 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize4
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_4 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 4 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize5
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_5 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 5 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize6
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_6 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 6 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize7
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_7 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 7 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize8
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_8 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 8 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize9
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_9 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 9 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize10
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_10 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 10 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize11
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_11 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 11 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize12
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_12 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 12 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize13
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_13 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 13 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize14
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_14 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 14 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize15
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_15 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 15 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize16
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_16 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 16 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize17
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_17 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 17 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize18
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_18 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 18 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize19
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_19 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 19 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetGhavaanajHerdSize20
	random = no
	
	text = {
		localisation_key = ghavaanajHerdElephant
		trigger = { ghavaanaj_has_herd_size_20 = yes }
	}
	text = {
		localisation_key = ghavaanajHerdPen
		trigger = { check_variable = { which = ghavaanaj_max_herd_size value = 20 } }
	}
	text = {
		localisation_key = ghavaanajHerdField
		trigger = { always = yes }
	}
}

defined_text = {
	name = GetBimLauSpiritPower
	random = no
	
	text = {
		localisation_key = BimLauTombPower_negligible
		trigger = {
			4565 = {
				NOT = {
					check_variable = {
						which = BimLauTombPower
						value = 25
					}
				}
			}
		}
	}
	
	text = {
		localisation_key = BimLauTombPower_very_weak
		trigger = {
			4565 = {
				check_variable = {
					which = BimLauTombPower
					value = 25
				}
				NOT = {
					check_variable = {
						which = BimLauTombPower
						value = 75
					}
				}
			}
		}
	}
	
	text = {
		localisation_key = BimLauTombPower_weak
		trigger = {
			4565 = {
				check_variable = {
					which = BimLauTombPower
					value = 75
				}
				NOT = {
					check_variable = {
						which = BimLauTombPower
						value = 125
					}
				}
			}
		}
	}
	
	text = {
		localisation_key = BimLauTombPower_medium
		trigger = {
			4565 = {
				check_variable = {
					which = BimLauTombPower
					value = 125
				}
				NOT = {
					check_variable = {
						which = BimLauTombPower
						value = 200
					}
				}
			}
		}
	}
	
	text = {
		localisation_key = BimLauTombPower_high
		trigger = {
			4565 = {
				check_variable = {
					which = BimLauTombPower
					value = 200
				}
			}
		}
	}
}

defined_text = {
	name = GetBimLauOpulence
	random = no
	
	text = {
		localisation_key = BimLauOpulence_negligible
		trigger = {
			4565 = {
				NOT = {
					check_variable = {
						which = BimLauTombOpulence
						value = 25
					}
				}
			}
		}
	}
	
	text = {
		localisation_key = BimLauOpulence_very_low
		trigger = {
			4565 = {
				check_variable = {
					which = BimLauTombOpulence
					value = 25
				}
				NOT = {
					check_variable = {
						which = BimLauTombOpulence
						value = 75
					}
				}
			}
		}
	}
	
	text = {
		localisation_key = BimLauOpulence_low
		trigger = {
			4565 = {
				check_variable = {
					which = BimLauTombOpulence
					value = 75
				}
				NOT = {
					check_variable = {
						which = BimLauTombOpulence
						value = 125
					}
				}
			}
		}
	}
	
	text = {
		localisation_key = BimLauOpulence_medium
		trigger = {
			4565 = {
				check_variable = {
					which = BimLauTombOpulence
					value = 125
				}
				NOT = {
					check_variable = {
						which = BimLauTombOpulence
						value = 200
					}
				}
			}
		}
	}
	
	text = {
		localisation_key = BimLauOpulence_high
		trigger = {
			4565 = {
				check_variable = {
					which = BimLauTombOpulence
					value = 200
				}
			}
		}
	}
}