# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
country_decisions = {
	azkare_check_cultural_representation = {
		potential = {
			OR = {
				tag = Y86
				was_tag = Y86
			}
			OR = { 
				has_reform = azkare_court
				has_reform = sunrise_court_reform
			}
			NOT = { has_country_flag = azkare_parliament_menu }
		}
		
		provinces_to_highlight = {
			owned_by = ROOT
			has_seat_in_parliament = yes
		}
		
		allow = {
			
		}
	
		effect = {
			custom_tooltip = azkare_convocation_event_happens_tt
			hidden_effect = {
				set_country_flag = azkare_parliament_menu
				country_event = { id = azkare.100 }
			}
		}
		
		ai_will_do = {
			factor = 0
		}
	}
	
	azkare_jaerelic_legions = {
		potential = {
			tag = Y00
			OR = { 
				has_reform = sunrise_court_reform
			}
			NOT = { has_country_flag = azkare_legions_menu }
		}
		
		allow = {
			
		}
	
		effect = {
			hidden_effect = {
				set_country_flag = azkare_legions_menu
				country_event = { id = azkare.111 }
			}
		}
		
		ai_will_do = {
			factor = 0
		}
	}
	
	azkare_elven_advisors_dec = { 
		potential = {
			tag = Y86
		}
		
		allow = {
			treasury = 50
			custom_trigger_tooltip = {
				tooltip = azkare_elf_advisor_tooltip
				NOT = {
					has_country_flag = azkare_recruited_elf_advisor_flag
				}
			}
		}
		
		effect = {
			add_treasury = -50
			country_event = {
				id = azkare.3
			}
		}
	}
	
	azkare_sarisung_handle_gangs_dec = {
		potential = {
			tag = Y86
			has_country_flag = azkare_rebuild_sarisung_flag_1
		}
		
		allow = {
			dip_power = 150
			mil_power = 150
			4500 = {
				has_manpower_building_trigger = yes
			}
			4489 = {
				has_manpower_building_trigger = yes
			}
			4470 = {
				has_manpower_building_trigger = yes
			}
			4364 = {
				has_manpower_building_trigger = yes
			}
			4620 = {
				has_manpower_building_trigger = yes
			}
		}
		
		effect = {
			add_dip_power = -150
			add_mil_power = -150
			set_country_flag = azkare_sarisung_gangs_flag
			clr_country_flag = azkare_rebuild_sarisung_flag_1
			sarisung_area = {
				remove_province_modifier = Y86_sarisung_belligerent_gangs
			}
		}
	}
	
	azkare_sarisung_capital_of_a_continent_dec = {
		potential = {
			tag = Y86
			has_country_flag = azkare_rebuild_sarisung_flag_2
		}
		
		allow = {
			4500 = {
				num_of_times_improved_by_owner = 10
				has_manufactory_trigger = yes
				has_trade_building_trigger = yes
				has_production_building_trigger = yes
				has_tax_building_trigger = yes
			}
			4489 = {
				num_of_times_improved_by_owner = 5
				has_manufactory_trigger = yes
				has_trade_building_trigger = yes
				has_production_building_trigger = yes
				has_tax_building_trigger = yes
			}
			4470 = {
				num_of_times_improved_by_owner = 5
				has_manufactory_trigger = yes
				has_trade_building_trigger = yes
				has_production_building_trigger = yes
				has_tax_building_trigger = yes
			}
			4364 = {
				num_of_times_improved_by_owner = 5
				has_manufactory_trigger = yes
				has_trade_building_trigger = yes
				has_production_building_trigger = yes
				has_tax_building_trigger = yes
			}
			4620 = {
				num_of_times_improved_by_owner = 5
				has_manufactory_trigger = yes
				has_trade_building_trigger = yes
				has_production_building_trigger = yes
				has_tax_building_trigger = yes
			}
		}
		
		effect = {
			set_country_flag = azkare_capital_of_a_continent_flag
			clr_country_flag = azkare_rebuild_sarisung_flag_2
			sarisung_area = {
				remove_province_modifier = Y86_sarisung_reluctant_subjects
			}
		}
	}
