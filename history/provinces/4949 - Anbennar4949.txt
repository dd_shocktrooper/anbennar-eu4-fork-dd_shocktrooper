owner = Y24
controller = Y24
add_core = Y24
add_core = Y21
culture = coastal_yan
religion = high_philosophy

hre = no

base_tax = 5
base_production = 4
base_manpower = 2

trade_goods = silk

capital = ""

is_city = yes
