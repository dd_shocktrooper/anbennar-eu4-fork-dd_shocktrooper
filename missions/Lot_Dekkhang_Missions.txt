lot_dekkhang_1 = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = Y58
	}
	has_country_shield = yes

	Y58_execute_azkare_infiltration = {
		icon = mission_whisper
		position = 1

		trigger = {
			OR = {
				alliance_with = Y62
				Y62 = { is_threat = ROOT }
				treasury = 55
			}
			if = {
				limit = { ai = no }
				has_estate_privilege = estate_daulophs_grand_mercenary_feast
				has_consort = no
			}
			exists = Y62
		}

		effect = {
			if = {
				limit = { NOT = { alliance_with = Y62 Y62 = { is_threat = ROOT } } }
				add_treasury = -55
			}
			add_estate_influence_modifier = {
				estate = estate_daulophs
				desc = "Sir Saetae Aided Courting: "
				influence = 5
				duration = 3650
			}
			Y62 = { country_event = { id = lotdekkhang.9 } }
		}
	}

#have azkare subjugated and tamed	
	Y58_subjugate_azkare = {
		icon = mission_unite_home_region
		position = 2
		required_missions = { Y58_execute_azkare_infiltration }

		trigger = {
			Y62 = {
				is_subject_of = ROOT 
				has_opinion = {
					who = ROOT
					value = 10
				}
				NOT = { liberty_desire = 50 }
				owns_core_province = 4790
				owns_core_province = 5425
			}
			treasury = 50
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4790
				province_id = 5425
			}
		}

		effect = {
			add_treasury = -50
			add_prestige = 10
			Y62 = { country_event = { id = lotdekkhang.5 } }
		}
	}
	
	Y58_sikai_unification = {
		icon = mission_plus_ultra
		position = 3
		required_missions = { Y58_subjugate_azkare }

		trigger = {
			Y62 = {
				is_subject_of = ROOT 
				ngoen_area = {
					type = all
					owned_by = Y62
					is_core = Y62
				}
				thirabnir_area = {
					type = all
					owned_by = Y62
					is_core = Y62
				}
				kaiden_area = {
					type = all
					owned_by = Y62
					is_core = Y62
				}
				phakphon_area = {
					type = all
					owned_by = Y62
					is_core = Y62
				}
			}
		}
		
		provinces_to_highlight = {
			OR = {
				area = ngoen_area
				area = thirabnir_area
				area = kaiden_area
				area = phakphon_area
			}
			NOT = {
				owned_by = Y62
			}
		}
		
		effect = {
			add_treasury = -50
			country_event = { id = lotdekkhang.6 }
			add_country_modifier = {
				name = Y58_lot_dekkhang_novice_overlord
				duration = -1 }
		}
	}
	
	Y58_defense_against_the_wulin = {
		icon = mission_great_wall
		position = 4
		required_missions = { Y58_sikai_unification }

		trigger = {
			Z62 = {
				is_subject_of = ROOT
				owns_core_province = 4789
				owns_core_province = 4787
				owns_core_province = 4940
				owns_core_province = 4777
			}
			4789 = { fort_level = 1 base_manpower = 5 }
			4940 = { fort_level = 1 base_manpower = 5 base_production = 5 }
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4789
				province_id = 4787
				province_id = 4940
				province_id = 4777
			}
		}
		
		effect = {
			country_event = { id = lotdekkhang.7 }
			country_event = { id = lotdekkhang.8 days = 10 }
		}
	}
	
	Y58_expanding_thangoya = {
		icon = mission_plus_ultra
		position = 5
		required_missions = { Y58_defense_against_the_wulin }

		trigger = {
			Z62 = { 
				is_subject_of = ROOT
				owns_core_province = 4942
				owns_core_province = 4776
				owns_core_province = 4775
				owns_core_province = 4759
			}
			phaktai_area = {
				type = all
				owned_by = Z62
				is_core = Z62
			}
			phonan_area = {
				type = all
				owned_by = Z62
				is_core = Z62
			}
			ananhu_area = {
				type = all
				owned_by = Z62
				is_core = Z62
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4942
				province_id = 4776
				province_id = 4775
				province_id = 4759
				area = phaktai_area
				area = phonan_area
				area = ananhu_area
			}
			NOT = {
				owned_by = Z62
			}
		}
		
		effect = {
			remove_country_modifier = Y58_lot_dekkhang_novice_overlord
			add_country_modifier = {
				name = Y58_lot_dekkhang_adept_overlord
				duration = -1 
			}
			
			ngaopho_area = { add_permanent_claim = Z62 }
			khalikhao_area = { add_permanent_claim = Z62 }
			kasainam_area = { add_permanent_claim = Z62 }
			yemakaibo_area = { add_permanent_claim = Z62 }
			if = {
				limit = {
					5424 = { owned_by = Y63 }
					Y63 = { is_subject = no }
				}
				country_event = { id = lotdekkhang.12 }
			}
		}
	}
	
	Y58_bokai_dominance = {
		icon = mission_plus_ultra
		position = 6
		required_missions = { Y58_expanding_thangoya }

		trigger = {
		treasury = 200
			Z62 = { 
				is_subject_of = ROOT
			}
				ngaopho_area = {
				type = all
				owned_by = Z62
				is_core = Z62
				}
				khalikhao_area = {
				type = all
				owned_by = Z62
				is_core = Z62
				}
				kasainam_area = {
				type = all
				owned_by = Z62
				is_core = Z62
				}
				yemakaibo_area = {
				type = all
				owned_by = Z62
				is_core = Z62
				}
		}
		
		provinces_to_highlight = {
			OR = {
				area = ngaopho_area
				area = khalikhao_area
				area = kasainam_area
				area = yemakaibo_area
			}
			NOT = {
				owned_by = Z62
			}
		}
		
		effect = {
		add_treasury = -200
		Z62 = { add_treasury = 200 }
			if = {
				limit = {
					NOT = {
						4752 = {
							OR = {	
								has_building = fort_15th
								has_building = fort_16th
								has_building = fort_17th
								has_building = fort_18th 
							}
						}
					}
				}
				4752 = {
					add_building_construction = {
						building = fort_15th
						speed = 0.5
						cost = 1
					}
				}
			}
			if = {
				limit = {
					4752 = { 
						has_building = fort_15th 
					}
				}
				4752 = {
					add_building_construction = {
						building = fort_16th
						speed = 0.5
						cost = 1
					}
				}
			}
			if = {
				limit = {
					4752 = {
						has_building = fort_16th 
					}
				}
				4752 = {
					add_building_construction = {
						building = fort_17th
						speed = 0.5
						cost = 1
					}
				}
			}
			if = {
				limit = {
					4752 = {
						has_building = fort_17th 
					}
				}
				4752 = {
					add_building_construction = {
						building = fort_18th
						speed = 0.5
						cost = 1
					}
				}
			}
			
			
			binhrunghin_area = { add_permanent_claim = Z62 }
			4742 = { add_permanent_claim = Z62 }
			4731 = { add_permanent_claim = Z62 }
			4729 = { add_permanent_claim = Z62 }
		}
	}
	
	Y58_kai_unification = {
		icon = mission_plus_ultra
		position = 7
		required_missions = { Y58_bokai_dominance }

		trigger = {
			Z62 = {
				is_subject_of = ROOT
				owns_core_province = 4742
				owns_core_province = 4731
				owns_core_province = 4729
			}
			binhrunghin_area = {
				type = all
				owned_by = Z62
				is_core = Z62
			}
		}
		
		provinces_to_highlight = {
			OR = {
				area = binhrunghin_area
				province_id = 4742
				province_id = 4731
				province_id = 4729
			}
			NOT = {
				owned_by = Z62
			}
		}
		
		effect = {
			remove_country_modifier = Y58_lot_dekkhang_adept_overlord
			add_country_modifier = {
				name = Y58_lot_dekkhang_skilled_overlord
				duration = -1 
			}
			country_event = { id = lotdekkhang.11 }
		}
	}	
}

lot_dekkhang_2 = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = Y58
	}
	has_country_shield = yes
	
	Y58_thikvot_scheme = {
		icon = mission_japanese_samurai
		position = 1

		trigger = {
			OR = {
				AND = {
					treasury = 40
					any_country = {
						has_reform = wulin
						has_spy_network_from = {
							who = Y58
							value = 25
						}
					}
				}
			}
		}

		effect = {
			add_treasury = -40
			Y40 = { country_event = { id = lotdekkhang.28 days = 30 } }
		}
	}
	
	Y58_teplin_faiths = {
		icon = mission_buddhist_monk_praying
		position = 2
		required_missions = { Y58_thikvot_scheme }

		trigger = {
			owns_core_province = 4793
			owns_core_province = 4941
			4941 = {
				OR = {
					has_building = temple
					has_building = cathedral
				}
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4793
				province_id = 4941
			}
			NOT = {
				OR = {
					AND = {
						province_id = 4793 
						owned_by = ROOT
					}
					AND = {
						province_id = 4941
						owned_by = ROOT
						OR = {
							has_building = temple
							has_building = cathedral
						}
					}
				}
			}
		}

		effect = {
			country_event = { id = lotdekkhang.29 }
		}
	}
	
	Y58_teplin_prosperity = {
		icon = mission_rice_field
		position = 3
		required_missions = { Y58_teplin_faiths }

		trigger = {
		stability = 1
		4941 = { owned_by = ROOT development = 40 OR = { has_building = marketplace has_building = trade_depot has_building = stock_exchange } }
		4605 = { owned_by = ROOT development = 15 OR = { has_building = workshop has_building = counting_house } }
		4795 = { owned_by = ROOT development = 15 OR = { has_building = workshop has_building = counting_house } }
		4947 = { owned_by = ROOT development = 15 OR = { has_building = workshop has_building = counting_house } }
		4793 = { owned_by = ROOT development = 15 OR = { has_building = workshop has_building = counting_house } }
		4796 = { owned_by = ROOT development = 15 OR = { has_building = barracks has_building = training_fields } }
		4797 = { owned_by = ROOT development = 15 OR = { has_building = barracks has_building = training_fields } }
		4799 = { owned_by = ROOT development = 15 OR = { has_building = workshop has_building = counting_house } }
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4941
				province_id = 4605
				province_id = 4795
				province_id = 4947
				province_id = 4793
				province_id = 4796
				province_id = 4797
				province_id = 4799
			}
			NOT = {
				OR = {
					AND = {
						province_id = 4941
						owned_by = ROOT
						development = 40
						OR = {
							has_building = marketplace has_building = trade_depot has_building = stock_exchange
						}
					}
					AND = {
						OR = {
							province_id = 4605
							province_id = 4795
							province_id = 4947
							province_id = 4793
							province_id = 4799
						}
						owned_by = ROOT
						development = 15
						OR = {
							has_building = workshop has_building = counting_house
						}
					}
					AND = {
						OR = {
							province_id = 4796
							province_id = 4797
						}
						owned_by = ROOT
						development = 15
						OR = {
							has_building = barracks has_building = training_fields
						}
					}
				}
			}
		}

		effect = {
			add_estate_loyalty = {
				estate = estate_daulophs
				loyalty = 10
			}
			4941 = { 
				add_prosperity = 25 
				if = {
					limit = { 	province_has_center_of_trade_of_level = 3 }
					add_base_production = 4 
				}
				else = {
					add_center_of_trade_level = 1 
				}
			}
			4797 = { add_prosperity = 25 }
		}
	}
	
	Y58_the_jungle_people = {
		icon = mission_clear_the_delta
		position = 4
		required_missions = { Y58_teplin_prosperity }

		trigger = {
			4798 = { owned_by = ROOT development = 15 OR = { has_building = barracks has_building = training_fields } }
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4798
			}
			NOT = {
				AND = {
					owned_by = ROOT
					development = 15
					OR = { has_building = barracks has_building = training_fields }
				}
			}
		}

		effect = {
			country_event = { id = lotdekkhang.30 }
		}
	}
	
	Y58_risbeko_tribes = {
		icon = mission_conquer_bengal_dlh
		position = 5
		required_missions = { Y58_the_jungle_people }

		trigger = {
			bomdan_region = {
				type = all
				OR = {
					owned_by = R38		
					owned_by = ROOT
					NOT = { culture = risbeko }
				}
			}
		}
		
		provinces_to_highlight = {
			bomdan_region = {
				type = all
			}
			NOT = {
				owned_by = ROOT
				owned_by = R38
				NOT = { culture = risbeko }
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "Y58_lot_dekkhang_jungle_experts"
				duration = -1
			}
			khangleu_thuing_area = { add_permanent_claim = ROOT }
			khabtei_teleni_area = { add_permanent_claim = ROOT }
			khindi_area = { add_permanent_claim = ROOT }
			4803 = { add_permanent_claim = ROOT }
			4804 = { add_permanent_claim = ROOT }
		}
	}
	
	Y58_raiders_and_priests = {
		icon = mission_junk_boat
		position = 6
		required_missions = { Y58_risbeko_tribes }

		trigger = {
			bomdan_region = {
				type = all
				OR = {
					owned_by = R38		
					owned_by = ROOT
					NOT = { culture = biengdi }
				}
			}
		}
		
		provinces_to_highlight = {
			bomdan_region = {
				type = all
			}
			NOT = {
				owned_by = ROOT
				owned_by = R38
				NOT = { culture = biengdi }
			}
		}
		
		effect = {
			country_event = { id = lotdekkhang.31 }
			phaemtuek_area = { add_permanent_claim = ROOT }
			nagon_area = { add_permanent_claim = ROOT }
			nui_phom_area = { add_permanent_claim = ROOT }
			khom_ma_area = { add_permanent_claim = ROOT }
			4699 = { add_permanent_claim = ROOT }
			4961 = { add_permanent_claim = ROOT }
			4701 = { add_permanent_claim = ROOT }
		}
	}
	
	Y58_the_gon_coast = {
		icon = mission_occupy_alexandria
		position = 7
		required_missions = { Y58_raiders_and_priests }

		trigger = {
			bomdan_region = {
				type = all
				OR = {	
					owned_by = ROOT
					NOT = { culture = gon }
				}
			}
			thidinkai_region = {
				type = all
				OR = {	
					owned_by = ROOT
					NOT = { culture = gon }
				}
			}
		}
		
		provinces_to_highlight = {
			OR = {
				bomdan_region = {
					type = all
				}
				thidinkai_region = {
					type = all
				}
			}
			NOT = {
				owned_by = ROOT
				NOT = { culture = gon }
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "Y58_lot_dekkhang_Sugar_Fields"
				duration = -1
			}
		}
	}
	
	Y58_the_ghost_emperors_land = {
		icon = mission_treasure_room
		position = 8
		required_missions = { Y58_the_gon_coast }

		trigger = {
			bomdan_region = {
				type = all
				OR = {	
					owned_by = ROOT
					NOT = { culture = khom }
				}
			}
			lupulan_rainforest_region = {
				type = all
				OR = {	
					owned_by = ROOT
					NOT = { culture = khom }
				}
			}
		}
		
		provinces_to_highlight = {
			OR = {
				bomdan_region = {
					type = all
				}
				lupulan_rainforest_region = {
					type = all
				}
			}
			NOT = {
				owned_by = ROOT
				NOT = { culture = khom }
			}
		}
		
		effect = {
			add_country_modifier = {
				name = "Y58_lot_dekkhang_ghost_emperor"
				duration = -1
			}
		}
	}
	
	Y58_bomdan_empire = {
		icon = mission_empire
		position = 8
		required_missions = { Y58_the_ghost_emperors_land Y58_porcelain_republic}

		trigger = {
            owns_all_provinces = {
                AND = {
                    culture_group = bom
                    continent = asia
                }
            }
        }
		
		provinces_to_highlight = {
            AND = {
                continent = asia
                NOT = {
                    owned_by = ROOT
                    NOT = { culture_group = bom }
                }
            }
        }
		
		effect = {
			set_government_rank = 4
		}
	}
}

lot_dekkhang_3 = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = Y58
	}
	has_country_shield = yes
	
	Y58_prepare_against_bim_lau = {
		icon = mission_reclaim_morocco
		position = 1

		trigger = {
			Y51 = {
				has_spy_network_from = {
					who = ROOT
					value = 20
				}
			}
			any_country = {
				alliance_with = ROOT
				is_rival = Y51
			}
		}

		effect = {
			add_prestige = 5
			add_dip_power = 50
			add_country_modifier = {
				name = "Y58_lot_dekkhang_spy_boost"
				duration = 3650
			}
		}
	}
	
	Y58_the_invasion_plan = {
		icon = mission_unite_home_region
		position = 2
		required_missions = { Y58_prepare_against_bim_lau }

		trigger = {
			Y51 = {
				has_spy_network_from = {
					who = ROOT
					value = 40
				}
			}
			any_country = {
				alliance_with = ROOT
				is_rival = Y51
			}
		}

		effect = {
			add_prestige = 5
			add_dip_power = 50
			middle_telebei_area = { add_permanent_claim = ROOT }
			4590 = { add_permanent_claim = ROOT }
			4589 = { add_permanent_claim = ROOT }
			4595 = {
				add_province_modifier = {
					name = Y58_lot_dekkhang_azkare_sabotaged_defenses
					duration = 1825
				}
			}
			add_spy_network_in = {
				who = Y51
				value = -40
			}
		}
	}
	
	Y58_down_the_telebei = {
		icon = mission_magadna_and_varanasi
		position = 3
		required_missions = { Y58_the_invasion_plan }

		trigger = {
			middle_telebei_area = {
				type = all
				owned_by = ROOT
				is_core = ROOT
			}
			owns_core_province = 4590
			owns_core_province = 4589
		}
		
		provinces_to_highlight = {
			OR = {
				area = middle_telebei_area
				province_id = 4590
				province_id = 4589
			}
			NOT = {
				AND = {
					owned_by = ROOT
					is_core = ROOT
				}
			}
		}

		effect = {
			add_prestige = 10
			phangban_area = { add_permanent_claim = ROOT }
			bim_lau_area = { add_permanent_claim = ROOT }
			4563 = { add_permanent_claim = ROOT }
		}
	}
	
	Y58_the_great_necropolis = {
		icon = mission_conquer_50_development
		position = 4
		required_missions = { Y58_down_the_telebei }

		trigger = {
			owns_core_province = 4565
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4565
			}
		}

		effect = {
			country_event = { id = lotdekkhang.26 }
		}
	}
	
	Y58_the_ranilau_people = {
		icon = mission_accomodate_the_rajputs
		position = 5
		required_missions = { Y58_the_great_necropolis }

		trigger = {
			prestige = 25
			legitimacy = 90
			owns_core_province = 4565
			4565 = { development = 40 } 
			NOT = { 4565 = { local_autonomy = 40 } }
			
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4565
			}
			
		}

		effect = {
			middle_telebei_area = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
			khung_btei_area = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
			4607 = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
			4590 = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
			4589 = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
			bim_lau_area = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
			lower_telebei_area = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
			reanthung_area = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
		}
	}
	
	Y58_suzerain_of_the_ranilau = {
		icon = mission_kowtow
		position = 6
		required_missions = { Y58_the_ranilau_people }

		trigger = {
			bomdan_region = {
				type = all
				OR = {
					owned_by = R38		
					owned_by = ROOT
					NOT = { culture = ranilau }
				}
			}
		}
		
		provinces_to_highlight = {
			bomdan_region = {
				type = all
			}
			NOT = {
				owned_by = ROOT
				owned_by = R38
				NOT = { culture = ranilau }
			}
		}

		effect = {
			li_btei_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			
			phangban_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			semphrerong_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	Y58_porcelain_republic = {
		icon = mission_market_place_with_asian_traders
		position = 8
		required_missions = { Y58_suzerain_of_the_ranilau }

		trigger = {
			bomdan_region = {
				type = all
				OR = {
					owned_by = R38		
					owned_by = ROOT
					NOT = { culture = chengrong }
				}
			}
		}
		
		provinces_to_highlight = {
			bomdan_region = {
				type = all
			}
			NOT = {
				owned_by = ROOT
				owned_by = R38
				NOT = { culture = chengrong }
			}
		}

		effect = {
			add_country_modifier = {
				name = "Y58_lot_dekkhang_porcelain_surplus"
				duration = -1
			}
			4364 = {
				if = {
					limit = {
						NOT = { is_core = ROOT }
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = ROOT
				}
			}
		}
	}
	
	Y58_fortifying_the_kharunyana = {
		icon = mission_great_wall
		position = 9
		required_missions = { Y58_porcelain_republic }

		trigger = {
			calc_true_if = {
				OR = {
					any_owned_province = {
						area = semphrerong_area
						fort_level = 2
						owned_by = ROOT
					}
				}
				OR = {
					any_owned_province = {
						area = phangban_area
						fort_level = 2
						owned_by = ROOT
					}
				}
				OR = {
					any_owned_province = {
						area = li_btei_area
						fort_level = 2
						owned_by = ROOT
					}
				}
				OR = {
					any_owned_province = {
						area = kharunyana_estuary_area
						fort_level = 2
						owned_by = ROOT
					}
				}
				OR = {
					any_owned_province = {
						area = reanthung_area
						fort_level = 2
						owned_by = ROOT
					}
				}
					amount = 4
			}
		}
		
		provinces_to_highlight = {
			OR = {
				area = semphrerong_area
				area = phangban_area
				area = li_btei_area
				area = kharunyana_estuary_area
				area = reanthung_area
			}
		}

		effect = {
			every_province = {
				LIMIT = {
					continent = asia
					trade_goods = chinaware
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
}

lot_dekkhang_4 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = Y58
	}
	has_country_shield = yes
	
	Y58_renovate_lapnam_slums = {
		icon = mission_asian_city
		position = 1

		trigger = {
			treasury = 20
			adm_power = 50
			owns_core_province = 4941
			4941 = {
				development = 30
				OR = {
					has_building = marketplace
					has_building = trade_depot
					has_building = stock_exchange
				}
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4941
			}
		}

		effect = {
			add_treasury = -20
			add_adm_power = -50
			medium_increase_of_hobgoblin_tolerance_effect = yes
			small_increase_of_harimari_tolerance_effect = yes
			4941 = { 
				add_base_tax = 4
				add_base_production = 1
				add_base_manpower = 1
				add_hobgoblin_minority_size_effect = yes
				add_harimari_minority_size_effect = yes
				}
				4941 = { add_prosperity = 25 }
			if = {
				limit = { is_institution_enabled = renaissance is_lacking_institutions = yes }
				4941 = {
					add_institution_embracement = {
						which = renaissance
						value = 30
					}
				}
			}
		}
	}
	
	Y58_drain_hobgoblin_gangs = {
		icon = mission_asian_city
		position = 2
		required_missions = { Y58_renovate_lapnam_slums }

		trigger = {
			4797 = {
				owned_by = ROOT
				OR = { has_building = barracks has_building = training_fields }
			}
			4941 = {
				owned_by = ROOT
			}
			OR = {
				medium_tolerance_hobgoblin_race_trigger = yes
				high_tolerance_hobgoblin_race_trigger = yes
			}
			mil_power = 100
			treasury = 50
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4797
			}
		}

		effect = {
			add_mil_power = -100
			add_treasury = -50
			4941 = { add_prosperity = 25  remove_province_modifier = y58_hobgoblin_slums }
			medium_increase_of_hobgoblin_tolerance_effect = yes
			country_event = { id = lotdekkhang.32 }

		}
	}
	
	Y58_control_the_dishonoured = {
		icon = mission_warrior_gold
		position = 3
		required_missions = { Y58_drain_hobgoblin_gangs }

		trigger = {
			4797 = {
				owned_by = ROOT
				OR = { has_building = barracks has_building = training_fields }
			}
			4941 = {
				owned_by = ROOT
			}
			mil_power = 100
			treasury = 50
			any_hired_mercenary_company = { home_province = { province_id = 4941 } }
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4797
			}
		}

		effect = {
			add_mil_power = -100
			add_treasury = -50
			random_hired_mercenary_company = {
				LIMIT = { home_province = {
					province_id = 4941 } }
						kill_mercenary_leader = THIS
				add_company_manpower = -0.25
			}
			4941 = { remove_province_modifier = y58_unregistered_dishonoured_mercs }
		}
	}
	
	Y58_solving_hobgoblin_murders = {
		icon = mission_whisper
		position = 4
		required_missions = { Y58_control_the_dishonoured }

		trigger = {
			any_hired_mercenary_company = { home_province = { province_id = 4797 } }
			any_hired_mercenary_company = { home_province = { province_id = 4941 } }
			adm_power = 20
			mil_power = 20
			treasury = 20
		}

		effect = {
			country_event = { id = lotdekkhang.36 }
		}
	}
	
	Y58_a_new_command = {
		icon = mission_restore_faith_in_the_throne
		position = 5
		required_missions = { Y58_solving_hobgoblin_murders }

		trigger = {
			custom_trigger_tooltip = { tooltip = lotdekkhang_solved_crimes_tt 
			has_country_flag = Y58_solved_crimes
			}
			ruler_culture = swallow_command
			ruler_religion = godlost
			any_hired_mercenary_company = { home_province = { province_id = 4797 } }
			any_hired_mercenary_company = { home_province = { province_id = 4941 } }
			army_size = R62
		}

		effect = {
			add_casus_belli = {
				target = R62
				type = cb_restore_personal_union
				months = 2400
			}
		}
	}
	
	Y58_stabilizing_the_command = {
		icon = mission_asian_trader
		position = 5
		required_missions = { Y58_a_new_command }

		trigger = {
			R62 = {
				is_subject_of = Y58 
			}
			R62 = {
				NOT = { liberty_desire = 50 }
			}
			R62 = {
				NOT = { average_unrest = 0 }
			}
			R62 = {
					num_of_rebel_armies = 0
			}
			R62 = {
				has_opinion = {
				who = ROOT
				value = 10
				}
			}
		}

		effect = {
			add_prestige = 30
			add_legitimacy = 20
			largest_increase_of_hobgoblin_tolerance_effect = yes
			country_event = { id = lotdekkhang.46 }
			
		}
	}
	
	Y58_integrate_hobgoblins_into_military = {
		icon = mission_manchu_warrior
		position = 5
		required_missions = { Y58_stabilizing_the_command }

		trigger = {
			mil_power = 200
			4797 = { owned_by = ROOT }
			4797 = { base_manpower = 20 }
			4797 = { has_building = training_fields }
			4797 = { OR = { has_building = regimental_camp has_building = conscription_center } }
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4797
			}
		}

		effect = {
			add_mil_power = -200
			largest_increase_of_hobgoblin_tolerance_effect = yes
			country_event = { id = lotdekkhang.47 }
		}
	}
	
	Y58_fate_of_the_brown_orcs = {
		icon = gory_to_the_black_army
		position = 5
		required_missions = { Y58_integrate_hobgoblins_into_military }

		trigger = {
			owns_core_province = 4605
			OR = {
				AND = {
					R62 = {
						is_subject_of = ROOT
						any_owned_province = { 
							culture = brown_orc
						}
					}
				}
				AND = {
					R62 = { is_subject_of = ROOT }
						OR = {
							R63 = { is_subject_of = R62 }
							R64 = { is_subject_of = R62 }		
						}
					}
				any_owned_province = { 
				culture = brown_orc
				}
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4605
			}
		}

		effect = {
			add_mil_power = -200
			largest_increase_of_orcish_tolerance_effect = yes
			country_event = { id = lotdekkhang.48 }
		}
	}
}

lot_dekkhang_5 = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = Y58
	}
	has_country_shield = yes
	
	Y58_unifying_the_daulophs = {
		icon = feast_of_pheasants
		position = 1

		trigger = {
			has_idea = Y58_the_dauloph_system
			owns_core_province = 4795
			estate_loyalty = {
				estate = estate_daulophs
				loyalty = 60
			}
			num_of_estate_privileges = {
				estate = estate_daulophs
				value = 4
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4795
			}
		}

		effect = {
			country_event = { id = lotdekkhang.40 }
		}
	}
	
	Y58_reforming_the_steel_binds = {
		icon = mission_scholar_officials
		position = 2
		required_missions = { Y58_unifying_the_daulophs }

		trigger = {
			estate_loyalty = {
				estate = estate_daulophs
				loyalty = 60
			}
				government_reform_progress = 50
				variable_arithmetic_trigger = {
				custom_tooltip = is_half_mercenary_army_tt
				export_to_variable = {
					which = our_army_size
					value = trigger_value:army_size
				}
				export_to_variable = {
					which = our_merc_size
					value = trigger_value:num_of_mercenaries
				}
				divide_variable = { which = our_merc_size which = our_army_size }
				check_variable = { which = our_merc_size value = 0.5 }
				#is_variable_equal = { which = our_army_size which = our_merc_size }
			}
			army_size_percentage = 1
		}

		effect = {
			set_country_flag = reformed_the_steel_binds
			add_government_reform = dauloph_mercenary_reform
			change_government_reform_progress = -50
		}
	}
	
	Y58_drain_harimari_slums = {
		icon = mission_conquer_bengal_tau
		position = 3
		required_missions = { Y58_reforming_the_steel_binds Y58_drain_hobgoblin_gangs }

		trigger = {
			4796 = {
				owned_by = ROOT
				OR = { has_building = barracks has_building = training_fields }
			}
			4941 = {
				owned_by = ROOT
			}
			OR = {
				medium_tolerance_harimari_race_trigger = yes
				high_tolerance_harimari_race_trigger = yes
			}
			mil_power = 200
			treasury = 100
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4796
			}
		}

		effect = {
			add_mil_power = -200
			add_treasury = -100
			4941 = { add_prosperity = 25  remove_province_modifier = y58_harimari_slums }
			medium_increase_of_harimari_tolerance_effect = yes
			country_event = { id = lotdekkhang.41 days = 60 }

		}
	}
	
	Y58_renovate_teplinbasiet = {
		icon = mission_fatehpur_sikri
		position = 4
		required_missions = {  Y58_drain_harimari_slums }

		trigger = {
			has_idea = Y58_teplinbasiet
			4941 = {
				owned_by = ROOT
			}
			adm_power = 200
			treasury = 500
			
			OR = {
				any_owned_province = { trade_goods = gems }
				any_subject_country = { any_owned_province = { trade_goods = gems } }
			}
			OR = {
				any_owned_province = { trade_goods = gold }
				any_subject_country = { any_owned_province = { trade_goods = gold } }
			}
			OR = {
				any_owned_province = { trade_goods = chinaware }
				any_subject_country = { any_owned_province = { trade_goods = chinaware } }
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4941
			}
		}

		effect = {
			add_adm_power = -200
			add_treasury = -500
			country_event = { id = lotdekkhang.42 days = 3650 }
		}
	}
	
	Y58_great_weapon_market = {
		icon = mission_conquer_nagaur
		position = 4
		required_missions = {  Y58_renovate_teplinbasiet }

		trigger = {
			has_idea = Y58_weapon_market_bookkeeping
			4941 = {
				owned_by = ROOT OR = {has_building = trade_depot has_building = stock_exchange}
			}
			treasury = 300
			
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4941
			}
		}

		effect = {
			add_treasury = -300
			country_event = { id = lotdekkhang.43 days = 3650 }

		}
	}
	
	Y58_khram_ptaa_academy = {
		icon = mission_consulate_of_the_sea
		position = 4
		required_missions = {  Y58_great_weapon_market }

		trigger = {
			has_idea = Y58_khram_ptaa_national_academy
			4941 = {
				owned_by = ROOT
			}
			treasury = 300
			
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 4941
			}
		}

		effect = {
			add_treasury = -300
			country_event = { id = lotdekkhang.44 days = 3650 }

		}
	}
}
